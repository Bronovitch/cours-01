/**
 * l'usage de library est interdit pour ces exercices
 */

/** 01 
*Afficher le nombre de caractère dans une chaine de caractère.
 */
let str = 'Bonjour';
console.log(str.length);

/** 02 mySplit
 * transformer la chaine de caractère suivante en tableau
 * "Bonjour j'adore l'informatique c'est vraiment super cool !"
 * la function doit avoir le même comportement que https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/String/split
 * tips: boucles
 */
let str2 = "Bonjour j'adore l'informatique c'est vraiment super cool !";

function mySplit(str) {
    const array = [];
    let tmp = '';
    for (let i = 0; i < str.length; i++) {
        if (str[i] !== ' ') {
            tmp += str[i];
        } else {
            array.push(tmp);
            tmp = '';
        }
    }
    return array;
}
console.log("mySplit ", mySplit(str2));


/** 03 myCleanStr
 * Transformer une chaine de caractère pour supprimer tous les espaces en trop
 * str = 'Bonjour  j'adore   l'informatique       c'est              vraiment super cool !"
 * tips: boucles
 */

let str3 = "Bonjour  j'adore   l'informatique       c'est              vraiment super cool     !"

function myCleanStr(str) {
    let tmp = '';
    let isFirstSpace = true;
    for (let i = 0; i < str.length; i++) {
        if (str[i] !== ' ') {
            tmp += str[i];
            isFirstSpace = true;
        } else {
            if (isFirstSpace) {
                tmp += ' ';
                isFirstSpace = false;
            } else {

            }
        }
    }
    return tmp;
}

console.log("myCleanStr =>", myCleanStr(str3))