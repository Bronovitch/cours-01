/** Base
 * Créer un array de fruit contenant au minimum 'banane', 'pomme', 'orange', 'pastèque', 'cerise'.
 * documentation: https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array
 */

const fruits = ['banane', 'pomme', 'orange', 'pastèque', 'cerise'];

/** exercice 1
* Ecrire une function qui affiche tous les fruits du tableau
* tips: boucle
*/
function display(array) {
    for (let item of array) {
        console.log(item);
    }
}
display(fruits);
/** exercice 2
* Rechercher dans le tableau et afficher 'pomme'
* Doc: https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array/find
*/
console.log("should return pomme => ", fruits.find((item) => {
    return item === 'pomme';
}))

/** exercice 3
 * Filtrer le tableau de fruit pour créer un autre tableau contenant tous les fruits sont le nom fait minimum 6 caractères de long
 * Doc: https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array/filter
 */

console.log("should return [ 'banane', 'orange', 'pastèque', 'cerise' ] => ", fruits.filter((item) => {
    return item.length >= 6;
}))