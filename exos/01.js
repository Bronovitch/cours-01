/**
 * l'usage de library est interdit pour ces exercices
 */

/** 01 myLength
*Afficher le nombre de caractère dans une chaine de caractère.
 */

/** 02 mySplit
 * transformer la chaine de caractère suivante en tableau
 * "Bonjour j'adore l'informatique c'est vraiment super cool !"
 * la function doit avoir le même comportement que https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/String/split
 * tips: boucles
 */

/** 03 myCleanStr
 * Transformer une chaine de caractère pour supprimer tous les espaces en trop
 * str = 'Bonjour  j'adore   l'informatique       c'est              vraiment super cool !"
 * tips: boucles
 */