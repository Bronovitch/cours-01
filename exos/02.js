/** Base
 * Créer un array de fruit contenant au minimum 'banane', 'pomme', 'orange', 'pastèque', 'cerise'.
 * documentation: https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array
 */

/** exercice 1
* Ecrire une function qui affiche tous les fruits du tableau
* tips: boucle
*/

/** exercice 2
* Rechercher dans le tableau et afficher 'pomme'
* Doc: https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array/find
*/

/** exercice 3 
 * Filtrer le tableau de fruit pour créer un autre tableau contenant tous les fruits sont le nom fait minimum 6 caractères de long
 * Doc: https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array/filter
 */