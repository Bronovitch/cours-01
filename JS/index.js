console.log("Hello World");
//https://gitlab.com/Bronovitch/cours-01
/** VARIABLE */

/* var */
var poid = 65;

/*string => 'Bonjour';
int (integer) =>5454;
float => 5.123;
double => 5.2;
Array => [0,1,2,3];
boolean => true/ false;*/
//var permet de déclarer une variable et de lui attribuer une valeur, à noter que var réagi differement selon l'OS

/* let */
let prenom = 'François';
prenom = 'Pierre';

//let permet de déclarer une variable et de lui attribuer une valeur, la variable sera disponible uniquement dans son scope

/* const */
sexe = 'Homme';

//const permet de déclarer une variable qui sera constante, c'est à dire que son type et valeur ne pourront pas changer.

/** CONDITIONS */

let age = 18;

if (age >= 18) {
    console.log("Je suis majeur!")
} else {
    console.log("Je suis mineur :(")
}


if (age >= 18 && age <= 62) {
    console.log("Je suis majeur et en age de travailler !")
} else if (age > 62) {
    console.log("Je suis à la retraite")
} else {
    console.log("je suis mineur !")
}

/** BOUCLE */

//une boucle sert à répéter une action

//Si je voulais compter jusqu'à 10 000 sans boucle je devrais écrire 10 000 fois la même chose comme ceci:

let nb = 0;

nb = nb + 1;
nb = nb + 1;
nb = nb + 1;
nb = nb + 1;
//etc jusqu'à ce que nb soit égale à 10 000
//les boucles sont là pour te sauver la vie:

while (nb < 10000) { // on peut le traduire par: Tant que nb est inférieur à 10 000 j'execute le code en dessous
    nb = nb + 1;
}

console.log(nb)
/**  SCOPE */

//Le scope représente la limite de visibilité d'une variable, la limite est souvent représenté par des accolades => {}

function display() {  //ouverture du scope 1
    let zero = 0;
    let testScope = "oui";
    if (zero === 0) { //ouverture du scope 2
        let un = 1;
        let zero = -1;
        console.log("Dans le scope 2 : un = ", un);
        console.log("Dans le scope 2 : zero = ", zero);
        console.log("Suis-je visible dans le scope 2 ? =>", testScope);
    } //fermeture du scope 2

    console.log("Dans le scope 1 : zero = ", zero)
    //console.log(`Décommenter cette ligne fera crash le script, car la variable ${un} n'a jamais été déclarée`);
}   //fermeture du scope 1
display();

